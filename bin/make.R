## ===============================================================
if(TRUE) {
  pn <- "alternativeROC"
  owa <- options()$warn ; options(warn=2)
  owd <- getwd() ; setwd(file.path(Sys.getenv("MYPKGSRC"),pn))
  cat("<<<< DOCUMENT >>>>\n")
  devtools::document(pn,roclets = c('rd', 'collate', 'namespace', 'vignette')) 
  cat("<<<< CHECK >>>>\n")
  devtools::check(pn,cran=TRUE,error_on="note") 
  cat("<<<< BUILD SOURCE >>>>\n")
  fn <- devtools::build(pn,binary=FALSE) 
  cat("<<<< INSTALL >>>>\n")
  try(detach(paste0("package:",pn),character.only=TRUE)) 
  install.packages(fn) ; require(pn,character.only=TRUE)
  cat("<<<< BUILD BINARY >>>>\n")
  fn <- devtools::build(pn,binary=TRUE) 
  cat("<<<< END >>>>\n")
  options(warn=owa)
  setwd(owd)
}
## ===============================================================
