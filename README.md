# R package: "alternativeROC"


## Description

The [R](https://r-project.org/) package `alternativeROC` provides a series of functions for the analysis of receiver operating characteristic curves (ROC curves, [Thomas et al. 2017](https://doi.org/10.1186/s41512-017-0017-y)).

## Installation

### From bitbucket

`alternativeROC` can be installed directly from bitbucket. 

```R
if (!require("devtools", quietly = TRUE)) install.packages("devtools")
devtools::install_bitbucket("SQ4/alternativeroc",subdir="alternativeROC")
```

For Windows users, RTools is required to install the package from bitbucket. RTools can be downloaded from [CRAN](https://cran.r-project.org/bin/windows/Rtools/). 

## Example

```R
require(alternativeROC)
## Create a ROC curve
set.seed(0)
count.neg <- count.pos <- 200
sd.neg <- sd.pos <- 1
mean.neg <- .5
mean.pos <- 1
skew.neg <- -8
skew.pos <- 0
x1 <- c(rsn(count.neg,mean.neg,sd.neg,skew.neg),
       rsn(count.pos,mean.pos,sd.pos,skew.pos))
       skew.neg <- 0
       skew.pos <- 8
x2 <- c(rsn(count.neg,mean.neg,sd.neg,skew.neg),
        rsn(count.pos,mean.pos,sd.pos,skew.pos))
y <- ordered(c(rep("neg",count.neg),rep("pos",count.pos)),levels=c("neg","pos"))
r1 <- roc(y,x1)
r2 <- roc(y,x2)

## Plot the ROC curve with an equi-PPV line
v <- plotROC(r1,ppv=0.2,prevalence=0.05,boot.n=1e2)
## AUC, PPV and sensitivity at PPV with confidence interval
print(v)

## Plot the ROC curve with an equi-NPV line
v <- plotROC(r2,npv=0.995,prevalence=0.05,boot.n=1e2)
## AUC, NPV and specificity at NPV with confidence interval
print(v)
```

