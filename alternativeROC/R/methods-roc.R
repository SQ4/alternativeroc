## ============================================================================
## Author:           Gregoire Thomas
## Created:          October 27, 2014
## Last Modified:    December 02, 2022
## Copyright (c) 2014-2022 SQU4RE  - http://www.squ4re.com/
## 
## This program is free software: you can redistribute it and/or modify it 
## under the terms of the GNU General Public License as published by the Free 
## Software Foundation, either version 3 of the License, or (at your option) 
## any later version.
##
## This program is distributed in the hope that it will be useful, but WITHOUT 
## ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
## FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
## details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
## ============================================================================

## =========================================================================
#' Diagnostic performance: Specificity from sensitivity, PPV and prevalence
#'
#' Diagnostic performance: Specificity from sensitivity, PPV and prevalence
#'
#' @inheritParams alternativeROC-common-args
#' @export 
#'
ppv.spatse <- function(se,ppv,prevalence) 1-(   se *   prevalence /(1-prevalence)*(1-ppv)/   ppv )
## =========================================================================

## =========================================================================
#' Diagnostic performance: Sensitivity from specificity, NPV and prevalence
#'
#' Diagnostic performance: Sensitivity from specificity, NPV and prevalence
#'
#' @inheritParams alternativeROC-common-args
#' @export 
#'
npv.seatsp <- function(sp,npv,prevalence) 1-(   sp *(1-prevalence)/   prevalence *(1-npv)/   npv )
## =========================================================================

## =========================================================================
#' Diagnostic performance: Sensitivity from specificity, PPV and prevalence
#'
#' Diagnostic performance: Sensitivity from specificity, PPV and prevalence
#'
#' @inheritParams alternativeROC-common-args
#' @export 
#'
ppv.seatsp <- function(sp,ppv,prevalence)    (1-sp)*(1-prevalence)/   prevalence *   ppv /(1-ppv)
## =========================================================================

## =========================================================================
#' Diagnostic performance: Specificity from sensitivity, NPV and prevalence
#'
#' Diagnostic performance: Specificity from sensitivity, NPV and prevalence
#'
#' @inheritParams alternativeROC-common-args
#' @export 
#'
npv.spatse <- function(se,npv,prevalence)    (1-se)*   prevalence /(1-prevalence)*   npv /(1-npv)
## =========================================================================

## =========================================================================
#' Diagnostic performance: Positive predictive values from a ROC curve
#'
#' Diagnostic performance: Positive predictive values from a ROC curve
#'
#' @inheritParams alternativeROC-common-args
#' @export
#'
ppv <- function(roc, prevalence)
  with(roc,cbind(threshold=c(thresholds),
               sensitivity=sensitivities,
               specificity=specificities,
               ppv=sensitivities * prevalence /
               (sensitivities * prevalence + (1-specificities) * (1-prevalence)),
               prevalence=rep(prevalence,length(sensitivities))))
## =========================================================================

## =========================================================================
#' Diagnostic performance: Negative predictive values from a ROC curve
#'
#' Diagnostic performance: Negative predictive values from a ROC curve
#'
#' @inheritParams alternativeROC-common-args
#' @export
#'
npv <- function(roc, prevalence)
  with(roc,cbind(threshold=c(thresholds),
               sensitivity=sensitivities,
               specificity=specificities,
               npv=specificities * (1-prevalence) /
               ((1-sensitivities) * prevalence + specificities * (1-prevalence)),
               prevalence=rep(prevalence,length(sensitivities))))
## =========================================================================

## =========================================================================
#' Diagnostic performance: Predictive values from a ROC curve
#'
#' Diagnostic performance: Predictive values from a ROC curve
#'
#' @inheritParams alternativeROC-common-args
#' @export
#'
pvs <- function(roc, prevalence)
  with(roc,cbind(threshold=c(thresholds),
               sensitivity=sensitivities,
               specificity=specificities,
               prevalence=rep(prevalence,length(sensitivities)),
               ppv=sensitivities * prevalence /
               (sensitivities * prevalence + (1-specificities) * (1-prevalence)),
               npv=specificities * (1-prevalence) /
               ((1-sensitivities) * prevalence + specificities * (1-prevalence)),
               prevalence=rep(prevalence,length(sensitivities))))
## =========================================================================

## =========================================================================
#' Diagnostic performance: Positive predictive value
#'
#' Diagnostic performance: Positive predictive value
#'
#' @inheritParams alternativeROC-common-args
#' @export 
#'
calc.ppv <- function(se=NULL, sp=NULL, ppv=NULL, prevalence, roc=NULL) {
  if(!is.null(roc)) {
    if(!is.null(ppv)) {
      p <- pvs(roc, prevalence)
      w <- which(p[,"ppv"]>ppv)
      w <- w[1]
      p[w,]#c("sensitivity","specificity")]
    } else if(!is.null(se)) {
      p <- pvs(roc, prevalence)
      w <- which(p[,"sensitivity"]>se)
      w <- w[length(w)]
      p[w,]
    } else stop("invalid input")
  } else if(is.null( se))    (1-sp)*(1-prevalence)/   prevalence *   ppv /(1-ppv)
  else   if(is.null( sp)) 1-(   se *   prevalence /(1-prevalence)*(1-ppv)/   ppv )
  else   if(is.null(ppv)) se*prevalence / (se*prevalence + (1-sp)*(1-prevalence))
  else stop("invalid input")
}
## =========================================================================

## =========================================================================
#' Diagnostic performance: Negative predictive value
#'
#' Diagnostic performance: Negative predictive value
#'
#' @inheritParams alternativeROC-common-args
#' @export 
#'
calc.npv <- function(se=NULL, sp=NULL, npv=NULL, prevalence, roc=NULL) {
  if(!is.null(roc)) {
    p <- npv(roc, prevalence)
    w <- which(p[,"npv"]>npv)
    w <- rev(w)[1]
    p[w,c("sensitivity","specificity")]
  } else if(is.null( se)) 1-(   sp *(1-prevalence)/   prevalence *(1-npv)/   npv )
  else   if(is.null( sp))    (1-se)*   prevalence /(1-prevalence)*   npv /(1-npv)
  else   if(is.null(npv)) sp*(1-prevalence) / ((1-se)*prevalence + sp*(1-prevalence))
  else stop("invalid input")
}
## =========================================================================

## =========================================================================
#' Diagnostic performance: Format AUC
#'
#' Diagnostic performance: Format AUC
#'
#' @param x Object of class ci.auc.
#' @param digits Precision.
#' @param show.method Show method.
#' @param ... Not used.
#' @export
#'
as.character.ci.auc <- function(x, digits = max(3, getOption("digits") - 3),
                                show.method=FALSE,...) {
  signif.ci <- format(x, digits = digits, nsmall = digits)
  ans <- ""
  ans <- paste(ans, "AUC:",signif.ci[2],
               ifelse(attr(attr(x, "auc"), "percent"), "%", ""), sep = "")
  ans <- paste(ans, " (", attr(x, "conf.level") * 100, "% CI: ", sep = "")
  ans <- paste(ans, signif.ci[1],
               ifelse(attr(attr(x, "auc"), "percent"), "%", ""), "-",
               signif.ci[3],
               ifelse(attr(attr(x, "auc"), "percent"), "%", ""), sep = "")
  if(show.method) {
    if (attr(x, "method") == "delong")
      ans <- paste(ans, ", DeLong)", sep = "")
    else
      ans <- paste(ans,", ", attr(x, "boot.n"), " ",
                   ifelse(attr(x, "boot.stratified"), "stratified", "non-stratified"),
                   " bootstrap replicates)", sep = "")
  } else ans <- paste(ans,")",sep="")
  ans
}
## =========================================================================

## =========================================================================
#' Diagnostic performance: Format specificity
#'
#' Diagnostic performance: Format specificity
#'
#' @param x Object of class ci.sp.
#' @param digits Precision.
#' @param show.method Show method.
#' @param ... Not used.
#' @export
#'
as.character.ci.sp <- function(x, digits = max(3, getOption("digits") - 3), 
                                show.method=FALSE,...) {
  signif.ci <- signif(x, digits = digits)
  ans <- ""
  ans <- paste(ans, "sens.:", signif(attr(x, "sensitivities"), digits = digits), sep="")
  ans <- paste(ans, " spec.:",signif.ci[,2], sep="")
  ans <- paste(ans, " (", attr(x, "conf.level") * 100, "% CI: ", sep = "")
  ans <- paste(ans, signif.ci[,1],
               ifelse(attr(attr(x, "auc"), "percent"), "%", ""), "-",
               signif.ci[,3],
               ifelse(attr(attr(x, "auc"), "percent"), "%", ""), sep = "")
  if(show.method) {
  #if (attr(x, "method") == "delong")
  #  ans <- paste(ans, ", DeLong)", sep = "")
  #else
    ans <- paste(ans,", ", attr(x, "boot.n"), " ",
                 ifelse(attr(x, "boot.stratified"), "stratified", "non-stratified"),
                 " bootstrap replicates)", sep = "")
  } else ans <- paste(ans,")",sep="")
  ans
}
## =========================================================================

## =========================================================================
#' Diagnostic performance: Format sensitivity
#'
#' Diagnostic performance: Format sensitivity
#'
#' @param x Object of class ci.se.
#' @param digits Precision.
#' @param show.method Show method.
#' @param ... Not used.
#' @export
#'
as.character.ci.se <- function(x, digits = max(3, getOption("digits") - 3), 
                               show.method=FALSE,...) {
  signif.ci <- signif(x, digits = digits)
  ans <- ""
  ans <- paste(ans, "spec.:", signif(attr(x, "specificities"), digits = digits), sep="")
  ans <- paste(ans, " sens.:",signif.ci[,2], sep="")
  ans <- paste(ans, " (", attr(x, "conf.level") * 100, "% CI: ", sep = "")
  ans <- paste(ans, signif.ci[,1],
               ifelse(attr(attr(x, "auc"), "percent"), "%", ""), "-",
               signif.ci[,3],
               ifelse(attr(attr(x, "auc"), "percent"), "%", ""), sep = "")
  if(show.method) {
  #if (attr(x, "method") == "delong")
  #  ans <- paste(ans, ", DeLong)", sep = "")
  #else
    ans <- paste(ans,", ", attr(x, "boot.n"), " ",
                 ifelse(attr(x, "boot.stratified"), "stratified", "non-stratified"),
                 " bootstrap replicates)", sep = "")
  } else ans <- paste(ans,")",sep="")
  ans
}
## =========================================================================

## =========================================================================
#' Diagnostic performance: Odd ratio
#'
#' Diagnostic performance: Odd ratio
#'
#' @inheritParams alternativeROC-common-args
#' @export
#'
diagnosticOR <- function (sp, se, prevalence)  {
  if(length(sp)!=length(se))
    stop("length of se and sp must match")
  tp <- se * prevalence
  fn <- (1-se) * prevalence
  tn <- sp * (1-prevalence)
  fp <- (1-sp) * (1-prevalence)
  ans <- (tp/fn)/(fp/tn)
  attributes(ans)$stderr.log <- sqrt(1/tp+1/fn+1/fp+1/tn)
  return(ans)
}
## =========================================================================

## =========================================================================
#' Diagnostic performance: Relative risk
#'
#' Diagnostic performance: Relative risk
#'
#' @inheritParams alternativeROC-common-args
#' @export
#'
diagnosticRR <- function (sp, se, prevalence)  {
  if(length(sp)!=length(se)) {
    print(list(se=se,sp=sp,prevalence=prevalence))
    stop("length of se and sp must match")
  }
  a <- se * prevalence
  c <- (1-se) * prevalence
  d <- sp * (1-prevalence)
  b <- (1-sp) * (1-prevalence)
  ans <- (a/(a+b))/(c/(c+d))
  attributes(ans)$OR <- a*d/b/c
  return(ans)
}
## =========================================================================

## =========================================================================
#' Diagnostic performance: Plot ROC curve
#'
#' Diagnostic performance: Plot ROC curve
#'
#' @param x Object of class roc.
#' @param annotate Annotate plot.
#' @param col.diagonal Color of the diagonal.
#' @param lty.diagonal Line type of the diagonal.
#' @param lwd.diagonal Line width of the diagonal.
#' @param col Color.
#' @param lwd Line width.
#' @param cex Size of the symbols.
#' @param ppv Positive predictive value cutoff
#' @param npv Negative predictive value cutoff
#' @param prevalence Prevalence of the positive outcome
#' @param col.pvs Color of the predictive value triangles
#' @param col.ci Color of the positive and negative predictive values.
#' @param lwd.ci Line width for the positive and negative predictive values.
#' @param len.ci Length of the end segment for positive and negative predictive values (see arrows).
#' @param boot.n Number of bootstrap replicates for the computation of the confidence interval of the specificity at NPV and of the sensitivity at PPV.
#' @param conf.level Width of the confidence interval of the specificity at NPV and of the sensitivity at PPV.
#' @param ... parameters to be passed to plot.
#' 
#' @import graphics
#' @importFrom sn rsn
#' 
#' @export
#' 
#' @examples
#' set.seed(0)
#' count.neg <- count.pos <- 200
#' sd.neg <- sd.pos <- 1
#' mean.neg <- .5
#' mean.pos <- 1
#' skew.neg <- -8
#' skew.pos <- 0
#' x <- c(sn::rsn(count.neg,mean.neg,sd.neg,skew.neg),
#'        sn::rsn(count.pos,mean.pos,sd.pos,skew.pos))
#' y <- ordered(c(rep("neg",count.neg),rep("pos",count.pos)),levels=c("neg","pos"))
#' r1 <- roc(y,x)
#' print(plotROC(r1,ppv=0.2,prevalence=0.05,boot.n=1e2))
#' skew.neg <- 0
#' skew.pos <- 8
#' x <- c(sn::rsn(count.neg,mean.neg,sd.neg,skew.neg),
#'        sn::rsn(count.pos,mean.pos,sd.pos,skew.pos))
#' y <- ordered(c(rep("neg",count.neg),rep("pos",count.pos)),levels=c("neg","pos"))
#' r2 <- roc(y,x)
#' print(plotROC(r2,npv=0.995,prevalence=0.05,boot.n=1e2))
#' 
#' 
plotROC <- function (x, annotate = FALSE, col.diagonal = "#00000080",
                     lty.diagonal = "solid",
                     lwd.diagonal = 1, col = "#303030", lwd = 2, cex = 2,
                     ppv=NULL,npv=NULL,prevalence=NULL,
                     col.pvs="#888888",
                     col.ci="#dd0000",lwd.ci=lwd,len.ci=.1,
                     boot.n=1e3,conf.level=.95,
                     ...) {
  quantiles <- (1-conf.level)/2*c(1,0,-1)+c(0,.5,1)
  if(is.na(boot.n)||(boot.n<2)) boot.n <- NULL
  ## ======
  ans <- list()
  v <- sort(ci(x,conf.level=0.95))[c(2,1,3)]
  names(v) <- c("50%","2.5%","97.5%")
  ans$AUC <- v
  ## ======
  pvs <- pvs(x,prevalence)
  cutpoint <- NULL
  ws <- c()
  if(!is.null(ppv)) {
    w <- which(pvs[,"ppv"]>ppv-.001)
    ws <- c(ws,w[1])
  } 
  if(!is.null(npv)) {
    w <- which(pvs[,"npv"]>npv-.001)
    if(length(w)) ws <- c(ws,rev(w)[1])
  }
  if(length(ws)) cutpoint <- as.data.frame(pvs)[ws,]
  ## ======
  graphics::plot(1:0, 0:1, xlim = 1:0, xlab = "specificity", ylab = "sensitivity",
       type = "l", col = col.diagonal, lty = lty.diagonal, lwd = lwd.diagonal,
       ...)
  if(!is.null(ppv)) {
    s <- ppv.spatse(0:1,ppv,prevalence)
    graphics::polygon(s[c(1,1,2)],c(1,0,1),col=paste(col.pvs,"44",sep=""),border=NA)
    graphics::lines(s,0:1,col=col.pvs,lwd=1,lty="solid")
  }
  if(!is.null(npv)) {
    s <- npv.seatsp(0:1,npv,prevalence)
    graphics::polygon(c(1,0,1),s[c(1,1,2)],col=paste(col.pvs,"44",sep=""),border=NA)
    graphics::lines(0:1,s,col=col.pvs,lwd=1,lty="solid")
  }
  olend <- graphics::par()$llend
  graphics::par(lend = 0)
  graphics::segments(0:5/5, rep(0, 6), 0:5/5, rep(1, 6), col = "grey",
           lty = "dotted", lwd = lwd.diagonal)
  graphics::segments(rep(0, 6), 0:5/5, rep(1, 6), 0:5/5, col = "grey",
           lty = "dotted", lwd = lwd.diagonal)
  graphics::par(lend = olend)
  if (!is.na(x[1])) {
    graphics::lines(x, col = col, lwd = lwd)
    if (annotate) {
      if ((length(x$cases)>3) && (length(x$controls)>3))
        graphics::mtext(side = 1, paste("", as.character(pROC::ci.auc(x),
                                               digits = 2), ""), adj = 1, line = -1.2, cex = 0.6,
              las = 0)
      else graphics::mtext(side = 1, paste("AUC:", format(x$auc,
                                                digits = 2, nsmall = 2), "(95% CI: N/A) ", sep = ""),
                 adj = 1, line = -1.2, cex = 0.6, las = 0)
    }
  }
  if(!is.null(cutpoint)&&is.null(boot.n)) graphics::points(cutpoint$specificity,cutpoint$sensitivity,
                               cex=cex,col=col,lwd=lwd)
  if(!is.null(boot.n)&&!is.null(ppv)) {
    ci <- ci.ppv(x,ppv=ppv,prevalence=prevalence,boot.n=boot.n,quantiles=quantiles)
    ans$PPV <- ppv ; ans$`sensitivity@PPV` <- ci
    e <- ppv.spatse(ci,ppv,prevalence)
    graphics::arrows(e[1:2],ci[1:2],e[2:3],ci[2:3],col=col.ci,lwd=lwd.ci,angle=90,code=3,length=len.ci)
  }
  if(!is.null(boot.n)&&!is.null(npv)) {
    ci <- ci.npv(x,npv=npv,prevalence=prevalence,boot.n=boot.n,quantiles=quantiles)
    ans$NPV <- npv ; ans$`specificity@NPV` <- ci
    e <- npv.seatsp(ci,npv,prevalence)
    graphics::arrows(ci[1:2],e[1:2],ci[2:3],e[2:3],col=col.ci,lwd=lwd.ci,angle=90,code=3,length=len.ci)
  }
  invisible(ans)
}
## =========================================================================

## =========================================================================
#' Diagnostic performance
#'
#' Diagnostic performance
#'
#' @inheritParams alternativeROC-common-args
#' @param ... Not used.
#' @import plyr
##' @importFrom pROC ci
#' @export 
#'
ci.ppv <- function(roc,ppv,prevalence,boot.n=2000,quantiles=c(0.5,.025,.975),...)
  stats::quantile(plyr::rdply(boot.n,stratified.ci.ppv(roc,ppv,prevalence,replace=TRUE))[,2],quantiles)
## ======
stratified.ci.ppv <- function (roc,ppv,prevalence,FUN=base::sample,...) {
  controls <- FUN(roc$controls,...)
  cases <- FUN(roc$cases,...)
  thresholds <- roc.utils.thresholds(c(cases, controls))
   perfs <- roc$fun.sesp(thresholds = thresholds, controls = controls,
                        cases = cases, direction = roc$direction)
  p <- perfs$se*prevalence/(perfs$se*prevalence+(1-perfs$sp)*(1-prevalence))
  return(ifelse(all(p<ppv,na.rm=TRUE),0,max(perfs$se[p>ppv],na.rm=TRUE)))
}
## =========================================================================

## =========================================================================
#' Diagnostic performance
#'
#' Diagnostic performance
#'
#' @inheritParams alternativeROC-common-args
#' @param ... Not used.
#' @import plyr
##' @importFrom pROC ci
#' @export 
#'
ci.npv <- function(roc,npv,prevalence,boot.n,quantiles=c(0.5,.025,.975),...)
  stats::quantile(plyr::rdply(boot.n,stratified.ci.npv(roc,npv,prevalence,replace=TRUE))[,2],quantiles)
## ======
stratified.ci.npv <- function (roc,npv,prevalence,FUN=base::sample,...) {
  controls <- FUN(roc$controls,...)
  cases <- FUN(roc$cases, ...)
  thresholds <- roc.utils.thresholds(c(cases, controls))
  perfs <- roc$fun.sesp(thresholds = thresholds, controls = controls,
                        cases = cases, direction = roc$direction)
  n <- perfs$sp*(1-prevalence)/((1-perfs$se)*prevalence+perfs$sp*(1-prevalence))
  return(ifelse(all(n<npv,na.rm=TRUE),0,max(perfs$sp[n>npv],na.rm=TRUE)))
}
## =========================================================================

