% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/methods-roc.R
\name{calc.npv}
\alias{calc.npv}
\title{Diagnostic performance: Negative predictive value}
\usage{
calc.npv(se = NULL, sp = NULL, npv = NULL, prevalence, roc = NULL)
}
\arguments{
\item{se}{Sensitivity.}

\item{sp}{Specificity.}

\item{npv}{Negative predictive value.}

\item{prevalence}{Prevalence.}

\item{roc}{Object of class pROC.}
}
\description{
Diagnostic performance: Negative predictive value
}
